<?php

/**
 * @file
 * Views for invitation module.
 */

/**
 * Implementation of hook_views_data
 */
function invitation_views_data() {
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['invitation']['table']['group'] = t('Invitation');
  $data['invitation']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Invitation'),
    'help' => t('The invitation ID'));
  $data['invitation']['id'] = array(
    'title' => t('ID'),
    'help' => t('The invitation ID'),
    'field' => array('handler' => 'views_handler_field'),
    'argument' => array('handler' => 'views_handler_argument_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort_numeric'));
  $data['invitation']['uid'] = array(
    'title' => t('UID'),
    'help' => t('The User ID'),
    'field' => array('handler' => 'views_handler_field'),
    'argument' => array('handler' => 'views_handler_argument_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort_numeric'));
  $data['invitation']['email'] = array(
    'title' => t('Email'),
    'help' => t('The mail of invitor'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ));
  $data['invitation']['request_code'] = array(
    'title' => t('Request code'),
    'help' => t('The request code'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ));
  $data['invitation']['invite_code'] = array(
    'title' => t('Invite code'),
    'help' => t('The invite code'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ));
  $data['invitation']['request_count'] = array(
    'title' => t('Invited count'),
    'help' => t('The number of user request from the ref URL'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ));


  return $data;
}
