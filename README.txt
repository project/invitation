README.txt
==========

Instruction
------------

Restricts registration to users who have an invite code 
(for running a private beta).

* The users do not have the invite code can request it by enter email.
* Each invite code only can be used once.
* The active users has a given number of invite code.
* The invite code can be generated by Admin

There are 2 ways to use this module. The invite code can be 

* Generated by admin at /admin/invitation/import or 
* Requested by the anonymous user. You can simplely disable this by not show
  the request code form block to users.

There are 2 blocks provided

* The invite request form block, anonymous use can enter there email 
  to get a reference URL to invite there friends to the website. Once
  there are 10 friends submit the request code form, the user will get
  the invite code to register. 
* The invite codes list block for logged in users, Every logged in 
  user has 5 invite code to invite friends.

You can create a view page to show the request code, invite code status

Steps to use this module:

1. Enable blocks *invitation_request* and *invitation_code* and emeb in your page
2. Enable View *invite_admin* and assign permission to the view
3. View the invitation code generated and usage at /admin/invitation


Author
-------

Plusfront <admin at plusfront DOT com>
http://plusfront.com
